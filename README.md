
SALAXY SINGLE SIGN-ON DEMO SITE (palkkaus-demo-sso)
===================================================

This project shows you how to implement Single Sign-on (federation) where
user logs into partner site using partner site login and partner authenticates
to Salaxy API. The process has the following steps in high level:

1. Partner registers as trusted partner (not needed in test) and obtains a certificate
2. Partner creates the customer company account thus getting access right as the creator of the account.
   - If, at this point, account has already been created account owner needs to give access rights.
3. Partner impersonates as customer company
4. Partner gets a Bearer token for customer company and Salaxy API is used with this token.

The demo service uses `storage/users.json` as mock-up "database" / user store.
This would be partner's own database.

The federeration simply takes place by requesting url `/federate/{userId}`, 
where the userId is the id of an existing user in the external system (e.g. 501).
The users.json contains the officialId for the user. 

The federation (code in the demo application) has the following steps:

- The Salaxy account will be created based on the officialId if it does not exist in the Salaxy system
- access_token will be generated
- the end user will be redirected to the single page application with access_token

Getting started
---------------

TypeScript version:

1. Clone repository `git clone https://gitlab.com/salaxy/salaxy-demo-sso`
2. Enter project directory `cd salaxy-demo-sso`
3. Run `npm install` for installing required grunt-modules
4. Build the project `grunt build`
5. Start the project web site in dev web server: `grunt server`
  - Server starts in the port 9001
6. Try the SSO with example account in: `http://localhost:9001`
7. Create your own account and certificates etc.
  - Read more in `storage/certificates/readme.md`
8. Explore the code in `/src/ts` folder

PHP version:

1. Follow the steps of TypeScript version
2. Go to `http://localhost:9001/php/index.php` for typescript version of the SSO functionality
3. Explore the code in `/src/php` folder

.NET version:

1. Clone repository `git clone https://gitlab.com/salaxy/salaxy-demo-sso`
2. Create a new web site using Internet Information Services (IIS) manager, set the `src\net` -directory as root
3. Try url: http://`<your site address>`/federate.aspx?userId=501
4. Have fun in exploring and developing with the code
5. Create your own account and certificates etc.
  - Read more in `storage/certificates/readme.md`
