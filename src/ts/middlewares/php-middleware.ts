import * as childProcess from "child_process";

/** Middleware for php samples. */
class PhpMiddleware {

  /**
   * Simple middleware for PHP processing.
   * @param path - Regular expression for url-paths to process.
   * @param phpCgi - Path to php-cgi.exe
   * @param serverRoot - Path to server root directory.
   */
  public middleware(path: RegExp, phpCgi: string, serverRoot: string) {

    const mw = (req: any, res: any, next: any) => {

      if (path.test(req.url)) {
        const parts = req.url.split("?");
        const phpFile = parts[0];
        let params = "";
        if (parts.length > 1) {
          params = " " + parts[1].split("&").join(" ");
        }

        const command = phpCgi + " " + serverRoot + phpFile + params;

        childProcess.exec(command, (error, stdout, stderr) => {
          if (error) {
            console.error(stderr);
          }
          let content = stdout;
          let status = null;
          const ehPos = stdout.indexOf("\r\n\r\n");
          if (ehPos > 0) {
            content = content.substr(ehPos + 4);
            const headerLines = stdout.substr(0, ehPos).split("\n");
            for (const headerLine of headerLines) {
              const divPos = headerLine.indexOf(":");
              if (divPos > 0) {
                const name = headerLine.substr(0, divPos);
                const value = headerLine.substr(divPos + 2).replace("\r", "");
                if (name.toLowerCase() === "status") {
                  status = value.split(" ")[0];
                } else {
                  res.setHeader(name, value);
                }
              }
            }
          }
          if (status) {
            res.writeHead(status);
          }
          res.write(content);
          res.end();
        });
      } else {
        next();
      }
    };
    return mw;
  }
}

/**
 * Simple middleware for PHP processing.
 * @param path - Regular expression for url-paths to process.
 * @param phpCgi - Path to php-cgi.exe
 * @param serverRoot - Path to server root directory.
 */
export function middleware(path: RegExp, phpCgi: string, serverRoot: string) {
  return new PhpMiddleware().middleware(path, phpCgi, serverRoot);
}
