import { DemoService, DummyUserDb } from "../demo";

/** Middleware for sso demo first page. */
class HomeMiddleware {

  /**
   *  Home page rendering
   */
  public middleware(path: RegExp) {

    const mw = (req: any, res: any, next: any) => {

      if (path.test(req.url)) {
        res.setHeader("Content-Type", "text/html; charset=utf-8");
        res.write(DemoService.getPageStart("Salaxy SSO demo"));
        res.write(`
          <p class="lead">
            The table below contains links to simple federation demo as well as IFrame demo
            implemented in TypeScript / JavaScript / NodeJS.
          </p>
          <p class="lead">
            Click here for <b><a href=\"/php/index.php\">PHP examples</a></b>.
            For <b>Asp.Net examples</b>, see the README on how to setup the demo in IIS.
          </p>
          <table>
            <tr><th>Internal id</th><th colspan="3">Name</th><th>Y-tunnus</th><th>Salaxy ID</th></tr>`);
        DummyUserDb.getMockDatabase().forEach((user) => {
          res.write(`<tr>
          <td>${user.userId}</td>
          <td>${user.name}</td>
          <td><a href="/federate/${user.userId}" target="_blank">Direct link (new window)</a></td>
          <td><a href="/iframe/0/0/${user.userId}">IFrame</a></td>
          <td>${user.officialId}</td>
          <td>${user.salaxyAccountId || "No id / federation done"}</td>
        </tr>`);
        });
        res.write("</table>");
        res.write(DemoService.getPageEnd());
        res.end();
      } else {
        next();
      }
    };
    return mw;
  }
}

/**
 * Home middleware
 */
export function middleware(path: RegExp) {
  return new HomeMiddleware().middleware(path);
}
