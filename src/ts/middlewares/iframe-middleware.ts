import { config } from "../config";
import { DemoService } from "../demo";

/**
 * Middleware for impersonation and federation as IFrame UI.
 * This is a bit different than the default Federation though the basic concepts are the same.
 */
class IFrameMiddleware {

  /**
   *  Please note that in this sample we give the userId as a parameter.
   *  In the real life, however, the userId must be obtained in a secure way after proper authorization.
   */
  public middleware(path: RegExp) {

    // Federate the given user and redirect the user with the new token to the site root
    const mw = (req: any, res: any, next: any) => {
      if (path.test(req.url)) {
        // Get userId parameter (for demop purposes only).
        // THIS IS NOT SECURE! You would need to get the userId in some other means in a real implementation.
        const urlParts = req.url.split("/");
        const userId = urlParts[urlParts.length - 1];
        const subSection = urlParts[urlParts.length - 2];
        const mainSection = urlParts[urlParts.length - 3];

        if (mainSection === "palkkaus") {
          // See the federate method for implementation details.
          DemoService.federate(userId)
          .then((token) => {
            renderIFramePage(res, config.targetSite, token, userId);
            return;
          })
          .catch((reason) => {
            if (reason.error.error_uri) {
              // This means that users needs to authorize the partner site in www.palkkaus.fi
              // Page in error_uri contains instructions for doing this.
              // If you want, you may add a redirect URL here where user should return after giving the access right.
              const location = reason.error.error_uri + "&redirect_uri=" + encodeURIComponent("https://www.google.com");
              res.writeHead(302, { Location: location });
              res.end();
              return;
            }
            // Rest of the errors should typically be just logged. Error messages do not make sense to end user.
            if (reason.error.error_description) {
              res.write("ERROR: " + reason.error.error_description);
            } else {
              res.write("Unknown error.");
            }
            res.end();
            return;
          });
        } else {
          // Rendering a demo page without federation.
          renderDemoPage(res, userId, Number(mainSection), Number(subSection));
        }
      } else {
        next();
      }
    };
    return mw;

    /** Renders a host service page for demonstration purpose */
    function renderDemoPage(res: any, userId: string, mainSection: number, subSection: number) {
      res.setHeader("Content-Type", "text/html; charset=utf-8");
      res.write(DemoService.getPageStart("Salaxy SSO - IFrame demo"));
      res.write(`
      <div class="row" style="height: calc(100% - 50px)">
        <div class="col-sm-4">
          <div class="salaxy-component salaxy-navi-sitemap">
            <ul class="list-group dummy-navi">
              <li class="list-group-item">
                <a href="/iframe/0/0/${userId}">Host site functionality</a>
                <ul class="${ !!mainSection ? "hidden" : "" }">
                  <li><a href="/iframe/0/1/${userId}">First subsection</a></li>
                  <li><a href="/iframe/0/2/${userId}">Subsection 2</a></li>
                  <li><a href="/iframe/0/3/${userId}">Subsection 3</a></li>
                </ul>
              </li>
              <li class="list-group-item">
                <a href="/iframe/palkkaus/0/${userId}">Palkkaus.fi</a>
              </li>
              <li class="list-group-item">
                <a href="/iframe/1/0/${userId}">Another section</a>
                <ul class="${ mainSection !== 1 ? "hidden" : "" }">
                  <li><a href="/iframe/1/1/${userId}">First subsection</a></li>
                  <li><a href="/iframe/1/2/${userId}">Subsection 2</a></li>
                  <li><a href="/iframe/1/3/${userId}">Subsection 3</a></li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
        <div class="col-sm-8">
          <h2>Main section ${mainSection}, subsection ${subSection}</h2>
          <p>
            This demo page marks a page in the hosting partner site.
            It has no Salaxy specific functionality, the iFrame and SSO integration have not been activated.
            Current user is ${userId}.
          </p>
          <p class="lead">
            Click. "Palkkaus.fi" on the left to start the SSO process to Palkkaus.fi.
          </p>
        </div>
      `);
      res.write(DemoService.getPageEnd());
      res.end();
    }

    /** Renders the HTML for page that has Salaxy site in IFrame. */
    function renderIFramePage(res: any, targetSite: string, token: string, userId: string) {
      res.setHeader("Content-Type", "text/html; charset=utf-8");
      res.write(DemoService.getPageStart("Salaxy SSO - IFrame demo"));
      const path = targetSite;
      const pathWithToken = config.targetSite + "#access_token=" + token;
      res.write(`
      <div class="row" style="height: calc(100% - 50px)">
        <div class="col-sm-4">
          <div class="salaxy-component salaxy-navi-sitemap">
            <ul class="list-group dummy-navi">
              <li class="list-group-item"><a href="/iframe/0/0/${userId}">Host site functionality</a></li>
              <li class="list-group-item">
                <a href="${path}#/" target="palkkausApp">Palkkaus.fi</a>
                <ul id="palkkausSubMenuLoader"><li>Ladataan...</li></ul>
                <ul id="palkkausSubMenu" style="display: none">
                  <li><a href="${path}#/welcome" target="palkkausApp">Yleiskuva</a></li>
                  <li><a href="${path}#/calc" target="palkkausApp">Laskelmat</a></li>
                  <li><a href="${path}#/payroll" target="palkkausApp">Palkkalistat</a></li>
                  <li><a href="${path}#/workers" target="palkkausApp">Ty&ouml;ntekij&auml;t</a></li>
                  <li><a href="${path}#/reports" target="palkkausApp">Raportit</a></li>
                  <li><a href="${path}#/settings" target="palkkausApp">Asetukset</a></li>
                </ul>
              </li>
              <li class="list-group-item"><a href="/iframe/1/0/${userId}">Another section</a></li>
            </ul>
          </div>
        </div>
        <div class="col-sm-8" style="position: relative">
          <script type="text/javascript">
            window.addEventListener("message", receiveMessage, false);
            function receiveMessage(event) {
              if (event && event.data) {
                switch (event.data.salaxySessionEvent) {
                  case "success":
                    // Show the menu
                    document.getElementById('palkkausSubMenu').style.display = 'block';
                    document.getElementById('palkkausSubMenuLoader').style.display = 'none';
                    return;
                  case "failure":
                    throw new Error('Salaxy authentication failed.');
                    return;
                  case "expired":
                    throw new Error('Salaxy authentication expired.');
                    return;
                  case "logout":
                    console.log('Salaxy authentication logout.')
                    return;
                }
              }
            }
          </script>
          <iframe id="palkkausApp" name="palkkausApp" src="${pathWithToken}" width="100%" height="100%"></iframe>
        </div>
      `);
      res.write(DemoService.getPageEnd());
      res.end();
    }
  }
}

/**
 * IFrame middleware
 */
export function middleware(path: RegExp) {
  return new IFrameMiddleware().middleware(path);
}
