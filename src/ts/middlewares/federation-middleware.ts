import { config } from "../config";
import { DemoService } from "../demo";

/** Middleware for impersonation and federation. */
class FederationMiddleware {

  /**
   *  Please note that in this sample we give the userId as a parameter.
   *  In the real life, however, the userId must be obtained in a secure way after proper authorization.
   */
  public middleware(path: RegExp) {

    // Federate the given user and redirect the user with the new token to the site root
    const mw = (req: any, res: any, next: any) => {
      if (path.test(req.url)) {
        // Get userId parameter
        const urlParts = req.url.split("/");
        const userId = urlParts[urlParts.length - 1];

        // See the federate method for implementation details.
        DemoService.federate(userId)
          .then((token) => {
            // Redirect to the site root with the new token
            res.writeHead(302, { Location: config.targetSite + "#access_token=" + token });
            res.end();
          })
          .catch((reason) => {
            if (reason.error.error_uri) {
              // This means that users needs to authorize the partner site in www.palkkaus.fi
              // Page in error_uri contains instructions for doing this.
              // If you want, you may add a redirect URL here where user should return after giving the access right.
              const location = reason.error.error_uri + "&redirect_uri=" + encodeURIComponent("https://www.google.com");
              res.writeHead(302, { Location: location });
              res.end();
              return;
            }
            // Rest of the errors should typically be just logged. Error messages do not make sense to end user.
            if (reason.error.error_description) {
              res.write("ERROR: " + reason.error.error_description);
            } else {
              res.write("Unknown error.");
            }
            res.end();
            return;
          });
      } else {
        next();
      }
    };
    return mw;
  }
}

/**
 * Federation middleware
 */
export function middleware(path: RegExp) {
  return new FederationMiddleware().middleware(path);
}
