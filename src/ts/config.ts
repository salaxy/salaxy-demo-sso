/**
 * Configuration for demo
 */
export const config = {
  apiServer: "https://test-api.salaxy.com",
  targetSite: "https://test.palkkaus.fi/company",
  // apiServer: "http://localhost:82",
  // targetSite: "http://localhost:9000",
  salaxyAccountId: "FI03POYS0006896389",
  salaxyCertificateFile: "certificate",
  salaxyCertificateThumbprint: "3852e850492280d9ce06d79dfcf1da6ef00e4ddc",
  salaxyCertificatePassword: "salaxy",
};
