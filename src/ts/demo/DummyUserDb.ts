import * as fs from "fs";
import * as path from "path";

import { DummyUserDbEntry } from "./DummyUserDbEntry";

/**
 * This class mocks a user database.
 * In a real implementation, this would be your own user database.
 */
export class DummyUserDb {

  /** Gets the mock-up database. */
  public static getMockDatabase(): DummyUserDbEntry[] {
    const data = fs.readFileSync(path.resolve(this.userDatabase));
    const users = JSON.parse(data.toString("utf8")) as DummyUserDbEntry[];
    return users;
  }

  /** Returns a user from the mock-up user database. */
  public static getUserInfo(userId: string): DummyUserDbEntry {
    const users = this.getMockDatabase();
    for (const user of users) {
      if (user.userId === userId) {
        return user;
      }
    }
    return null;
  }

  /** Saves user to the user database. */
  public static saveUserInfo(user: DummyUserDbEntry): void {
    const data = fs.readFileSync(path.resolve(this.userDatabase));
    const users = JSON.parse(data.toString("utf8")) as DummyUserDbEntry[];

    for (let i = 0; i < users.length; i++) {
      if (users[i].userId === user.userId) {
        users[i] = user;
        break;
      }
    }
    fs.writeFileSync(path.resolve(this.userDatabase), JSON.stringify(users));
  }

  // Stores user data in this site
  private static userDatabase = "www/storage/users.json";

}
