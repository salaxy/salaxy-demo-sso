/** User database entity. */
export class DummyUserDbEntry {

    /**
     * User ID in the Partner's own database.
     */
    public userId: string;

    /**
     * Official id (Y-tunnus). The partner site should know this:
     * The Palkkaus.fi account is created based on this ID.
     * We fetch the data form Company registry (YTJ.fi)
     */
    public officialId: string;

    /**
     * Salaxy account id. Saved here after first call to AssureCompanyAccount.
     */
    public salaxyAccountId: string;

    /** Company name: Not used in integration, just for demo purposes. */
    public name: string;
}
