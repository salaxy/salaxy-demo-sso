import {
  PartnerCompanyAccountInfo,
} from "@salaxy/core";

import { config } from "../config";
import { DummyUserDb, DummyUserDbEntry } from "../demo";
import { SalaxySsoService } from "../helpers";

/**
 * Logic that is internal for the demo application.
 * Abstracted from the base cod eto keep the examples compact.
 */
export class DemoService {

  /**
   * Does the federation: Gets a token for the specified user.
   * @param userId User / company ID in the internal database.
   */
  public static federate(userId: string): Promise<string> {
    const ssoService = new SalaxySsoService(config);
    // First assure that the company will be created if it does not exist
    return this.assureCompanyAccount(userId)
      .then((company) => {
        // Create assertion token for the company using Salaxy account id
        const assertionToken = ssoService.getAssertionToken(company.salaxyAccountId);
        // Request a new token for the company using the assertion token
        return ssoService.getToken(assertionToken);
      })
      .then((tokenMessage) => {
        // Return the received token
        return tokenMessage.access_token;
      });
  }

  /** Assures that the company will be created if it does not exist */
  public static assureCompanyAccount(userId: string): Promise<DummyUserDbEntry> {
    const ssoService = new SalaxySsoService(config);
    // Get the user from your own database.
    const user = DummyUserDb.getUserInfo(userId);
    if (user == null) {
      throw Error(`User ${userId} is not in your own database.`);
    }

    if (user.salaxyAccountId) {
      // We have stored the account ID in our own database => The AssureCompanyAccount has already been called.
      // So we could really skip the assureCompanyAccount here like this:
      // return Promise.resolve(user);
    }
    return ssoService.assureCompanyAccount({
      officialId: user.officialId,
      partnerAccountId: user.userId,
      contactFirstName: "John", // These should also come from your database.
      contactLastName: "Snow",
      email: "winter@salaxy.com",
      telephone: "+358401234567",
    }).then((partnerCompanyAccountInfo: PartnerCompanyAccountInfo) => {

      // Check this if you use partnerAccountId
      const isAuthorized = partnerCompanyAccountInfo.isAuthorized;

      // Save the returned data to our own user database
      user.salaxyAccountId = partnerCompanyAccountInfo.accountId;
      DummyUserDb.saveUserInfo(user);
      return user;
    });
  }

  /** Gets the beginning of the page: Head and start of the body with title. */
  public static getPageStart(title: string) {
    return `
    <html>
    <head>
      <title>${ title }</title>
      <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,400italic" rel="stylesheet" type="text/css">
      <link href="https://developers.salaxy.com/dist/v02/css/bootstrap.css" rel="stylesheet" type="text/css" />
      <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
      <link href="https://developers.salaxy.com/dist/v02/css/salaxy-lib-ng1-bootstrap.css" rel="stylesheet" type="text/css" />
    </head>
    <body class="salaxy-component">
      <div class="container">
        <div class="bg-primary" style="padding: 10px">
          <h1>${ title }<a href="/" class="btn btn-default pull-right">Home</a></h1>
        </div>
      `;
  }

  /** Gets the ending of the page. */
  public static getPageEnd() {
    return "</div></body></html>";
  }
}
