
import {sign} from "jsonwebtoken";

/**
 * Utility class for creating an assertion token from the certificate for given account
 */
export class CertificateUtility {

    /**
     * Creates an assertion token
     *
     * @param thumbprint Salaxy certificate thumbprint
     * @param certificate Certificate contents as string.
     * @param privateKey Private key as string or Buffer.
     * @param issuer Issuer of the certificate
     * @param expires Date when the certificate should expire.
     * @param accountId Target account ID that we sign in as.
     * @param audience Audience of the token. E.g. "https://secure.salaxy.com/oauth2/token"
     */
    public static createAssertionToken(
      thumbprint: string,
      certificate: string,
      privateKey: string|Buffer,
      issuer: string,
      expires: Date,
      accountId: string,
      audience: string,
      ): string {

        // Read and parse certificate
        const startTag = "-----BEGIN CERTIFICATE-----";
        const endTag = "-----END CERTIFICATE-----";
        const start = certificate.indexOf(startTag) + startTag.length;
        const end = certificate.indexOf(endTag);
        certificate = certificate.substring(start, end).replace(/(?:\r\n|\r|\n)/g, "");

        // Make header with certificate data
        const buffer = Buffer.from(thumbprint, "hex");
        let x5t = buffer.toString("base64");
        x5t = x5t.replace(/\+/g, "-").replace(/\//g, "_").replace(/=+$/, "");

        const headers = {
            algorithm: "RS256",
            header: {
                x5t,
                x5c: [certificate],
            },
        };

        // Make payload
        const payload = {
            iss: issuer,
            sub: accountId,
            aud: audience,
            exp: Math.floor(expires.getTime() / 1000),
        };
        // Sign certificate
        const token = sign(payload, privateKey, headers);
        return token;
    }
}
