import * as fs from "fs";
import * as path from "path";

import {
  AssureCompanyAccountRequest,
  OAuth2,
  OAuthGrantType,
  OAuthMessage,
  PartnerCompanyAccountInfo,
} from "@salaxy/core";

import {
  AjaxNode,
} from "@salaxy/node";

import { CertificateUtility } from "./CertificateUtility";

/**
 * Provides methods for performing the Single Sign-on functionality in Salaxy environment.
 */
export class SalaxySsoService {

  /** Ajax client for connecting to Salaxy API. */
  private ajax: AjaxNode;

  // TODO: Certificate store
  private certificateStore = "www/storage/certificates";

  // TODO: Issuer
  private issuer = "http://tempuri.org";

  /** Default constructor creates a new SalaxySsoService with a configuration  */
  constructor(private config: {
    apiServer: string,
    /** Target site for federation */
    targetSite: string,
    /** Our own Salaxy account id */
    salaxyAccountId: string,
    /** The certificate file name without extension */
    salaxyCertificateFile: string,
    /** Certificate thumbprint */
    salaxyCertificateThumbprint: string,
    salaxyCertificatePassword: string,
    /** Certificate store */
    certificateStore?: string,
  }) {
    this.ajax = new AjaxNode();
    this.ajax.serverAddress = config.apiServer;
    this.certificateStore = (config.certificateStore || this.certificateStore);
  }

  /**
   * Create assertion token for given Salaxy account id.
   * This can be done with certificate (and private key) locally without hhtp calls.
   * @param salaxyAccountId Target account ID that we want to authenticate as.
   */
  public getAssertionToken(salaxyAccountId: string): string {
    // The thumbprint of the certificate
    const thumbprint = this.config.salaxyCertificateThumbprint;
    // This can be partner specific
    const expires = new Date(Date.now() + (10 * 24 * 60 * 60 * 1000));
    const baseFilePath = path.resolve(this.certificateStore, this.config.salaxyCertificateFile);
    const crt = fs.readFileSync(baseFilePath + ".crt");
    const pvk = fs.readFileSync(baseFilePath + ".pvk");
    const aud = this.config.apiServer + "/oauth2/token";
    const assertionToken = CertificateUtility.createAssertionToken(thumbprint, crt.toString(), pvk, this.issuer, expires, salaxyAccountId, aud);
    return assertionToken;
  }

  /** Assures that the company account exists. */
  public assureCompanyAccount(request: AssureCompanyAccountRequest): Promise<PartnerCompanyAccountInfo> {
    // Create assertion token for self
    const assertionToken = this.getAssertionToken(this.config.salaxyAccountId);
    // Use assertion token to generate a new token for SELF
    return this.getToken(assertionToken).then((tokenMessage) => {
      // Configure the API client with the token
      const ajax = this.getAjax(tokenMessage.access_token);
      // Post data to API
      return ajax.postJSON("/partner/company", request);
    });
  }

  /**
   * Gets the Bearer token from Salaxy server using assertion token.
   * @param assertionToken The assertion token can be created with the certificate.
   */
  public getToken(assertionToken: string): Promise<OAuthMessage> {
    const oauth2 = new OAuth2(this.getAjax());
    const oAuthRequest: OAuthMessage = {
      grant_type: OAuthGrantType.JwtBearer,
      assertion: assertionToken,
    };
    return oauth2.token(oAuthRequest);
  }

  /**
   * Returns the API client.
   * @param token If specified, sets the token. Otherwise the ajax client is anonymous.
   */
  public getAjax(token?: string): AjaxNode {
    const ajax = new AjaxNode();
    ajax.serverAddress = this.config.apiServer;
    if (token) {
      ajax.setCurrentToken(token);
    }
    return ajax;
  }

}
