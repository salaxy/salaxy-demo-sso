set PATH=%PATH%;"C:\cygwin64\bin"
set CERT_PWD=salaxy

for %%a in ("*.pfx") do call :extract %%~na
goto :EOF

:extract
del %1.pvk
del %1.crt

openssl pkcs12 -in "%1.pfx"  -nodes -out "%1.pvk" -nocerts -password pass:"%CERT_PWD%"
openssl pkcs12 -in "%1.pfx"  -nodes -out "%1.crt" -nokeys -password pass:"%CERT_PWD%"

:EOF