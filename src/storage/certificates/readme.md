The certificates here allow you access to test environment:

- TEST: https://test.palkkaus.fi
- Account: FI03 POYS 0006 8963 89 (SSO Rauta-Maatalous Oy, 0689638-9)
- Credentials: sso-test@palkkaus.fi / test
- Cert name in https://test-pro.palkkaus.fi: salaxy-demo-sso
- Thumbprint: 3852e850492280d9ce06d79dfcf1da6ef00e4ddc
- Password: salaxy

The account has couple of child accounts 

Before doing any significant testing (adding/removing objects), please create your own certificate.

Creating your own certificate:

1. Go to the appropriate web site:
   - https://test.palkkaus.fi for testing
   - https://www.palkkaus.fi for production
2. Create an account if you do not already have one
   - Create a company site
   - In prodcution, you need the signature rights for this part
3. Sign in with your account
4. Create a certificate
   - Asetukset
   - Valtuutukset ja varmenteet
   - Lisää uusi
   - Lataa sertifikaatti pfx-tiedostona
   - For e.g. php and typeScript you also need the Thumbprint (id in the UI).
5. For, php or TypeScript convert pfx-file to separate public and private keys:
   - This is not necessary in .Net: Windows native libraries can use pfx directly)  
     But e.g. Node.js or PHP-libraries typically cannot use pfx-files.
   - Install OpenSSL (For Windows e.g. https://slproweb.com/products/Win32OpenSSL.html)
   - Modify OpenSSL path and CERT_PWD in extract.bat
   - extract.bat => .crt and .pvk

Please ensure in production that private key is always in safe place and not accessible.
