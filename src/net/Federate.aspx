﻿<%@ Import Namespace="Microsoft.IdentityModel.Tokens" %>
<%@ Import Namespace="System.IdentityModel.Tokens.Jwt" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.Security.Claims" %>
<%@ Import Namespace="System.Security.Cryptography.X509Certificates" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<script language="c#" runat="server">
        // User database is mocked by a JSON file.
        private string userDatabase = "storage/users.json";
        // Relative path to the certificate. In production, makse sure you have it in secure location.
        private string certificatePath = "../storage/certificates/certificate.pfx";
        //Password for pfx
        private string salaxyCertificatePassword = "salaxy";
        // Our own Salaxy account id
        private string ownSalaxyAccountId = "FI03POYS0006896389";
        // Api server
        private string apiServer = "https://demo-secure.salaxy.com";
        // Target site
        private string targetSite = "https://demo-www.palkkaus.fi/company";


        /** User mapping class */
        public class UserInfo
        {
            public string userId;
            public string officialId;
            public string salaxyAccountId;
            public string name;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            var userId = Request.Params["userId"];
            if (userId != null)
            {
              try 
              {
                var token = federate(userId);
                Response.Redirect(targetSite +"#access_token=" + token);
              }
              catch (System.Net.WebException ex)
              {
                var response = new StreamReader(ex.Response.GetResponseStream()).ReadToEnd();
                dynamic responseObject = JsonConvert.DeserializeObject(response);
                if (responseObject.error_uri != null) 
                {
                  // This means that users needs to authorize the partner site in www.palkkaus.fi
                  // Page in error_uri contains instructions for doing this.
                  // If you want, you may add a redirect URL here where user should return after giving the access right.
                  string location = responseObject.error_uri + "&redirect_uri=" + Server.UrlEncode("https://www.google.com/");
                  Response.Redirect(location);
                }
                throw ex;
              }
            }
            else
            {
                Response.Write("Please give userId -parameter in the query string.");
            }
        }

        /** Utility method for creating an assertion token from the certificate for given account */
        private string createAssertionToken(byte[] certificateBytes, string password, string issuer, DateTime expires, string accountId)
        {
            //Load certificate
            var certificate = new X509Certificate2(certificateBytes, password, X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.Exportable);

            //Make header, add signing certificate
            var header = new JwtHeader(new SigningCredentials(new X509SecurityKey(certificate), "RS256"));
            header.Add(JwtHeaderParameterNames.X5c, new string[] { Convert.ToBase64String(certificate.RawData) });

            //Make payload
            var payload = new JwtPayload(
                issuer,
                apiServer +"/oauth2/token",
                new Claim[] { new Claim("sub", accountId) },
                null,
                expires);

            //Make token and sign
            var token = new JwtSecurityToken(header, payload);

            //Serialize
            var tokenHandler = new JwtSecurityTokenHandler();
            string jwt = tokenHandler.WriteToken(token);
            return jwt;
        }

        /** Federates the given company user and returns a new token for the user. */
        private string federate(string userId)
        {
            // First assure that the company will be created if it does not exist
            var company = assureCompanyAccount(userId);
            // Create assertion token for the company using Salaxy account id
            var assertionToken = getAssertionToken(company.salaxyAccountId);
            // Request a new token for the company using the assertion token
            var token = getToken(assertionToken);
            return token; 
        }

        /** Create assertion token for given Salaxy account id */
        private string getAssertionToken(string salaxyAccountId)
        {
            // This can be partner specific
            var issuer = "http://tempuri.org";
            var expires = DateTime.Now.AddMilliseconds(24 * 60 * 60 * 1000);
            var bytes = File.ReadAllBytes(Path.Combine(Server.MapPath("."), certificatePath));
            var assertionToken = createAssertionToken(bytes, salaxyCertificatePassword, issuer, expires, salaxyAccountId);
            return assertionToken;
        }

        /** Get bearer token using assertion */
        private string getToken(string assertionToken)
        {
            var oAuthRequest = new
            {
                grant_type = "urn:ietf:params:oauth:grant-type:jwt-bearer",
                assertion = assertionToken
            };
            var wc = new WebClient();
            wc.Headers[HttpRequestHeader.ContentType] = "application/json";
            wc.Encoding = Encoding.UTF8;
            var tokenUrl = apiServer +"/oauth2/token";
            var tokenResponseJson = wc.UploadString(tokenUrl, JsonConvert.SerializeObject(oAuthRequest));
            dynamic tokenResponse = JsonConvert.DeserializeObject(tokenResponseJson);
            return tokenResponse.access_token;
        }

        /** Assures that the company will be created if it does not exist */
        private UserInfo assureCompanyAccount(string userId)
        {

            // Check if the user exists in our own database
            var user = getUserInfo(userId);
            if (user == null)
            {
                throw new Exception("this should never happen");
            }

            // Create assertion token for self
            var assertionToken = getAssertionToken(ownSalaxyAccountId);

            // Use assertion token to generate a new token for self
            var token = getToken(assertionToken);

            // Create request object
            var assureCompanyAccountRequest = new
            {
                officialId = user.officialId,
                partnerAccountId = user.userId
            };
            // Post data to API
            var wc = new WebClient();
            wc.Headers[HttpRequestHeader.ContentType] = "application/json";
            wc.Headers[HttpRequestHeader.Authorization] = "Bearer " + token;
            wc.Encoding = Encoding.UTF8;
            var assureCompanyUrl = apiServer +"/v03/api/partner/company";

            var assureCompanyResponseJson = wc.UploadString(assureCompanyUrl, JsonConvert.SerializeObject(assureCompanyAccountRequest));

            dynamic assureCompanyResponse = JsonConvert.DeserializeObject(assureCompanyResponseJson);

            // Check this if you use partnerAccountId;
            var isAuthorized = assureCompanyResponse.isAuthorized;

            user.salaxyAccountId = assureCompanyResponse.accountId;
            saveUserInfo(user);
            return user;
        }

        /** Returns a user from the user database. */
        private UserInfo getUserInfo(string userId)
        {
            var data = File.ReadAllText(Path.Combine(Server.MapPath("."),"../"+userDatabase));
            var users = JsonConvert.DeserializeObject<UserInfo[]>(data);
            foreach (var user in users)
            {
                if (user.userId == userId)
                {
                    return user;
                }
            }
            return null;
        }

        /** Saves user to the user database. */
        private void saveUserInfo(UserInfo user)
        {
            var data = File.ReadAllText(Path.Combine(Server.MapPath("."),"../"+userDatabase));
            var users = JsonConvert.DeserializeObject<UserInfo[]>(data);

            for (var i = 0; i < users.Length; i++)
            {
                if (users[i].userId == user.userId)
                {
                    users[i] = user;
                    break;
                }
            }
            File.WriteAllText(Path.Combine(Server.MapPath("."),"../"+userDatabase), JsonConvert.SerializeObject(users));
        }
    
</script>
