<html>
  <head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
  <body>
    <title>SSO demo</title>
    <table  class="table table-dark">
      <thead>
        <tr><th>Id</th><th>Y-tunnus</th><th></th><th></th><th>Salaxy-id</th></tr>
      </thead>
      <tbody>
      <tr>
          <td>501</td>
          <td>E.S Solutions (1531230-6)</td>
          <td><a href="federate.php?userId=501">Federate</a></td>
          <td><a href="iframe.php?userId=501">IFrame</a></td>
          <td>FI38PTME0015312306</td>
      </tr>
      <tr>
          <td>502</td>
          <td>Insinööritoimisto S. Sonninen Oy (0559989-2)</td>
          <td><a href="federate.php?userId=502">Federate</a></td>
          <td><a href="iframe.php?userId=502">IFrame</a></td>
          <td>FI03POYI0005599892</td>
      </tr>
      <tr>
          <td>503</td>
          <td>S & S Soini Oy (1064478-2)</td>
          <td><a href="federate.php?userId=503">Federate</a></td>
          <td><a href="iframe.php?userId=503">IFrame</a></td>
          <td>FI88POYS0010644782</td>
      </tr>
     </tbody>
    </table>
  </body>
</html>

