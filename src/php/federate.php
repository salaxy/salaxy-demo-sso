<?php 

require __DIR__ . '/vendor/autoload.php';

use \Firebase\JWT\JWT;

// Stores user data in this site
$user_database = "../storage/users.json";
// Certificate store
$certificate_store = "../storage/certificates";
// The thumbprint of the Salaxy certificate which is used for impersonation
$salaxy_certificate_file = "certificate";
// The thumbprint of the Salaxy certificate which is used for impersonation
$salaxy_certificate_thumbprint = "3852e850492280d9ce06d79dfcf1da6ef00e4ddc";
// Our own Salaxy account id
$own_salaxy_account_id = "FI03POYS0006896389";
// Api server
$api_server = "https://test-api.salaxy.com";
// Target site
$target_site = "https://test.palkkaus.fi/company";

redirect();

function redirect() {
    global $target_site;
    $token = federate($_GET["userId"]);  
    header("Location: ".$target_site."#access_token=".$token); 
    exit(); 
}

/** Federates the given company user and returns a new token for the user. */
function federate($user_id)
{
    // First assure that the company will be created if it does not exist
     $company = assure_company_account($user_id);
    // Create assertion token for the company using Salaxy account id
     $assertion_token = get_assertion_token($company["salaxyAccountId"]);
    // Request a new token for the company using the assertion token
     $token = get_token($assertion_token);
    //return token;
    return $token;
}

/** Assures that the company will be created if it does not exist */
function assure_company_account($user_id)
{
    global $api_server;
    global $own_salaxy_account_id;

    // Check if the user exists in our own database
    $user = get_user_info($user_id);
   
    // Create assertion token for self
    $assertion_token = get_assertion_token($own_salaxy_account_id);

    // Use assertion token to generate a new token for self
    $token = get_token($assertion_token);

    $service_url = $api_server."/v02/api/partner/company";
    // Create request object
    $data = array(
        "officialId" => $user["officialId"],
        "partnerAccountId" => $user["userId"]
    );
    // Post data to API
    $payload = json_encode($data);  
    $ch = curl_init($service_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        "Authorization: Bearer ".$token,
        'Content-Type: application/json',
        'Content-Length: ' . strlen($payload))
    );
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $response = curl_exec($ch);
    curl_close($ch);
    $result = json_decode($response, true);

    // check this if you use partnerAccountId
    $isAuthorized =  $result["isAuthorized"];
    
    $user["salaxyAccountId"] = $result["accountId"];
    save_user_info($user);
    return $user;
}

/** Returns a user from the user database. */
function get_user_info($user_id)
{
  global $user_database;
  $users = json_decode(file_get_contents(realpath($user_database)), true);
  foreach ($users as $user)
  {
      if ($user["userId"] == $user_id)
      {
          return $user;
      }
  }
  return null;
}

 /** Saves user to the user database. */
function save_user_info($user)
 {
    global $user_database;
    $users = json_decode(file_get_contents(realpath($user_database)), true);
    foreach ($users as &$storage_user)
    {
        if ($storage_user["userId"] == $user["userId"])
        {
            $storage_user = $user;
        }
    }
    $data = json_encode($users);
    file_put_contents(realpath($user_database), $data);
 }

/** Create assertion token for given Salaxy account id */
function get_assertion_token($salaxy_account_id)
{
  global $certificate_store;
  global $salaxy_certificate_file;
  global $salaxy_certificate_thumbprint;

    // This can be partner specific
    $issuer = "http://tempuri.org";
    $expires = time() + (10 * 24 * 60 * 60 ) ;
    
    $base_file_path = $certificate_store."\\".$salaxy_certificate_file;

    $crt = file_get_contents(realpath($base_file_path.".crt"));
    $pvk = file_get_contents(realpath($base_file_path.".pvk"));

    $assertion_token = create_assertion_token($salaxy_certificate_thumbprint, $crt, $pvk, $issuer, $expires, $salaxy_account_id);

    return $assertion_token;
}

/** Utility method for creating an assertion token from the certificate for given account */
function create_assertion_token($thumbprint, $certificate, $private_key, $issuer, $expires, $account_id)
{
    global $api_server;

    // Read and parse certificate
    $startTag = "-----BEGIN CERTIFICATE-----";
    $endTag = "-----END CERTIFICATE-----";
    $start = strpos($certificate, $startTag) + strlen($startTag);
    $end = strpos($certificate, $endTag);
    $crt_data = substr($certificate, $start, $end - $start);
    $crt_data = preg_replace("/(?:\r\n|\r|\n)/", "", $crt_data);

    // Make header with certificate data
    $thumbprint = base64_encode(pack("H*",$thumbprint));
    $thumbprint = preg_replace("/\+/", "-", $thumbprint);
    $thumbprint = preg_replace("/\//", "_", $thumbprint);
    $thumbprint = preg_replace("/=+$/", "", $thumbprint);

    $header = array(
          "x5t" => $thumbprint,
          "x5c" =>  [$crt_data]
    );

    // Make payload
    $payload = array(
        "iss" => $issuer,
        "sub" => $account_id,
        "aud" => $api_server."/oauth2/token",
        "exp" => $expires
    );
    // Sign certificate
  
    $jwt = JWT::encode($payload, $private_key, 'RS256', null, $header);
    return $jwt;
}

/** Get bearer token using assertion */
function get_token($assertion_token) {

    global $api_server;
    $service_url = $api_server."/oauth2/token";
    $data = array(
        "grant_type" => "urn:ietf:params:oauth:grant-type:jwt-bearer",
        "assertion" => $assertion_token
    );
    $payload = json_encode($data);  
    $ch = curl_init($service_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($payload))
    );
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $response = curl_exec($ch);
    curl_close($ch);

    $result = json_decode($response, true);

    return $result["access_token"];
}


?>