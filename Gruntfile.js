module.exports = function(grunt) {
    require('jit-grunt')(grunt);

    grunt.initConfig({
        gruntServerPort: 9001,
        ts: {
            server: {
                src: [
                    'src/ts/**/*.ts'
                ],
                outDir: 'www/js',
                options: {
                    module: 'commonjs',
                    target: 'es5',
                    sourceMap: false,
                    declaration: false,
                    removeComments: false,
                    moduleResolution: "node",
                    rootDir: "./src/ts",
                    skipLibCheck: true,
                }
            },
        },
        copy: {
            storage: {
                files: [
                    { expand: true, cwd: 'src', src: ['storage/**'], dest: 'www/' },
                ],
            },
            php: {
                files: [
                    { expand: true, cwd: 'src', src: ['php/**'], dest: 'www/' },
                ],
            },
            html: {
                files: [
                    { expand: true, cwd: 'src', src: ['**/*.html'], dest: 'www/' },
                ],
            },
        },
        connect: {
            all: {
                options: {
                    port: '<%= gruntServerPort%>',
                    base: 'www',
                    hostname: "localhost",
                    livereload: true,
                    middleware: function(connect, options) {

                        const serveStatic = require("serve-static");
                        const serveIndex = require("serve-index");
                                          
                        // Same as in grunt-contrib-connect
                        var middlewares = [];
                        if (!Array.isArray(options.base)) {
                          options.base = [options.base];
                        }

                        const path = require("path");
                        
                        const homeMiddleware = require(path.resolve(options.base[0] + "/js/middlewares/home-middleware.js"));
                        const federationMiddleware = require(path.resolve(options.base[0] + "/js/middlewares/federation-middleware.js"));
                        const iframeMiddleware = require(path.resolve(options.base[0] + "/js/middlewares/iframe-middleware.js"));
                        const phpMiddleware = require(path.resolve(options.base[0] + "/js/middlewares/php-middleware.js"));

                        middlewares.push(homeMiddleware.middleware(new RegExp('^\/$')));
                        middlewares.push(federationMiddleware.middleware(new RegExp('^\/federate\/')));
                        middlewares.push(iframeMiddleware.middleware(new RegExp('^\/iframe\/')));
                        middlewares.push(phpMiddleware.middleware(new RegExp('\.php'),  path.resolve(options.base[0] + "/bin/php-cgi.exe"), path.resolve(options.base[0])));

                        // Options for serve-static module. See https://www.npmjs.com/package/serve-static
                        var defaultStaticOptions = {};
                        var directory = options.directory || options.base[options.base.length - 1];
                        options.base.forEach(function(base) {
                        // Serve static files.
                        var path = base.path || base;
                        var staticOptions = base.options || defaultStaticOptions;
                        middlewares.push(serveStatic(path, staticOptions));
                        });
                        // Make directory browse-able.
                        // middlewares.push(serveIndex(directory.path || directory));
                        return middlewares;
                    }
                }
            },
        },
        watch: {
            all: {
                files: ['www/**/*.*', '!www/storage/**/*.json'],
                options: {
                    livereload: 35728
                },
            },
            ts: {
                files: ['src/**/*.ts'],
                tasks: ['ts'],
            },
            php: {
                files: ['src/php/**/*.*'],
                tasks: ['copy:php'],
            },
            html: {
                files: ['src/**/*.html'],
                tasks: ['copy:html'],
            },
        },
        open: {
            all: {
                path: 'http:/<%= connect.all.options.hostname%>:<%= connect.all.options.port%>/'
            }
        },
    });

    // ===========================================================================
    // BUILD JOBS 		  ========================================================
    // =========================================================================== 
    var tasks = ['ts','copy'];
   
    grunt.registerTask('default', tasks);
    grunt.registerTask('build', tasks);
    grunt.registerTask('server',  ['connect', 'open', 'watch']);
};