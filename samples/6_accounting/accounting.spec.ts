import { expect } from "chai";

import { AccountingData} from "@salaxy/core";

import {
  SalaxySsoService,
} from "../_utils/SalaxySsoService";

// Configuration
import * as config from "../_configs/config.json";

// Create ajax
const ssoService = new SalaxySsoService(config);

describe("Accounting", () => {
  
  it("should get accounting report for salary", async () => {
    const ajax = await ssoService.assureCompanyAndGetAjax({officialId: config.sampleBusinessId});

    const filter = `otherPartyInfo/officialId eq '${config.sampleWorkerOfficialId}' and status eq 'paymentSucceeded'`;
    const list = await ajax.getJSON(`/v03-rc/api/calculations/all?$filter=${encodeURIComponent(filter)}`);
    
    expect(list.items.length).above(0);

    const accountingData: AccountingData =  await ajax.getJSON(`/v03-rc/api/reports/accounting?calculationIds=${list.items[0].id}`);

    console.log(accountingData);
    expect(accountingData || null).not.null;
    
  }).timeout(60000); 
});

