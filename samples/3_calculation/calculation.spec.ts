import { expect } from "chai";

import { Calculation, CalculationRowType, PaymentChannel} from "@salaxy/core";

import {
  SalaxySsoService,
} from "../_utils/SalaxySsoService";

// Configuration
import * as config from "../_configs/config.json";

// Create ajax
const ssoService = new SalaxySsoService(config);

describe("Calculation", () => {
  
  it("should list calculations for the worker", async () => {
    const ajax = await ssoService.assureCompanyAndGetAjax({officialId: config.sampleBusinessId});
    const filter = `otherPartyInfo/officialId eq '${config.sampleWorkerOfficialId}'`;
    const list = await ajax.getJSON(`/v03-rc/api/calculations/all?$filter=${encodeURIComponent(filter)}`);
 
    expect(list).not.null;
  }).timeout(60000);; 

  it("should create a new calculation for the worker", async () => {
    const ajax = await ssoService.assureCompanyAndGetAjax({officialId: config.sampleBusinessId});
    // Get employment
    const workerFilter = `otherPartyInfo/officialId eq '${config.sampleWorkerOfficialId}'`;
    const employmentList = await ajax.getJSON(`/v03-rc/api/accounts/workers?$filter=${encodeURIComponent(workerFilter)}`);
    expect(employmentList.items.length).eq(1);

    const filter = `otherPartyInfo/officialId eq '${config.sampleWorkerOfficialId}' and status eq 'draft'`;
    const list = await ajax.getJSON(`/v03-rc/api/calculations/all?$filter=${encodeURIComponent(filter)}`);
    const calc = list.items.length > 0 ? await ajax.getJSON(`/v02/api/calculations/${list.items[0].id}`) as Calculation :
    {
      worker: {
        accountId: employmentList.items[0].otherId,
        employmentId: employmentList.items[0].id,
        tax: { }
      },
      rows: [
        {
          rowType: CalculationRowType.Compensation,
          count: 1,
          price: 500
        }
      ],
      info: {
        paymentChannel: PaymentChannel.PalkkausManual,
      },
      workflow: {
       // requestedSalaryDate
      }
    } as Calculation;

    const updatedCalc = await ajax.postJSON("/v02/api/calculations", calc); 

    expect(updatedCalc || null).not.null;

  }).timeout(60000);; 
});

