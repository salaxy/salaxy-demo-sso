import { expect } from "chai";

import { Calculation, Invoice, InvoiceStatus, InvoiceType } from "@salaxy/core";

import {
  SalaxySsoService,
} from "../_utils/SalaxySsoService";

// Configuration
import * as config from "../_configs/config.json";

// Create ajax
const ssoService = new SalaxySsoService(config);

describe("Invoice", () => {
  
  it("should create invoice from the calculation", async () => {
    const ajax = await ssoService.assureCompanyAndGetAjax({officialId: config.sampleBusinessId});

    const filter = `otherPartyInfo/officialId eq '${config.sampleWorkerOfficialId}' and status eq 'draft'`;
    const list = await ajax.getJSON(`/v03-rc/api/calculations/all?$filter=${encodeURIComponent(filter)}`);
    
    expect(list.items.length).above(0);

    const calc =  await ajax.getJSON(`/v02/api/calculations/${list.items[0].id}`) as Calculation

    const invoices: Invoice[] = await ajax.postJSON(`/v03-rc/api/invoices/create-by-id/${calc.info.paymentChannel}`, [calc.id]); 
    
    const netInvoices = invoices.filter( x => x.header.type == InvoiceType.Net);
    expect(netInvoices.length).equal(1);

  }).timeout(60000); 

  it("should query net invoice for the calculation and get sepa data", async () => {
    const ajax = await ssoService.assureCompanyAndGetAjax({officialId: config.sampleBusinessId});

    const filter = `otherPartyInfo/officialId eq '${config.sampleWorkerOfficialId}' and status eq 'paymentStarted'`;
    const list = await ajax.getJSON(`/v03-rc/api/calculations/all?$filter=${encodeURIComponent(filter)}`);
    
    expect(list.items.length).above(0);
    
    // Query invoices
    const invoiceList = await ajax.getJSON(`/v03-rc/api/invoices?$filter=${encodeURIComponent(`entityType eq 'net' and businessObjects/any(b: b eq '${list.items[0].id}')`)}`);
    expect(invoiceList.items.length).equal(1);

    // Get sepa
    const response = await ajax.getJSON(`/v03-rc/api/invoices/sepa?ids=${invoiceList.items[0].id}`); 
    console.log(response);
    
  }).timeout(60000); 

  it("should update the status of invoice to paid", async () => {
    const ajax = await ssoService.assureCompanyAndGetAjax({officialId: config.sampleBusinessId});

    const filter = `otherPartyInfo/officialId eq '${config.sampleWorkerOfficialId}' and status eq 'paymentStarted'`;
    const list = await ajax.getJSON(`/v03-rc/api/calculations/all?$filter=${encodeURIComponent(filter)}`);
    
    expect(list.items.length).above(0);

    // Query invoices
    const invoiceList = await ajax.getJSON(`/v03-rc/api/invoices?$filter=${encodeURIComponent(`entityType eq 'net' and businessObjects/any(b: b eq '${list.items[0].id}')`)}`);
    expect(invoiceList.items.length).equal(1);

    // Get invoice
    const invoice: Invoice = await ajax.getJSON(`/v03-rc/api/invoices/${invoiceList.items[0].id}`); 
    expect(invoice).not.null;
    expect(invoice.header.status).not.equal(InvoiceStatus.Paid);

    // Update status
    const paidInvoice: Invoice = await ajax.postJSON(`/v03-rc/api/invoices/${invoiceList.items[0].id}/status/${InvoiceStatus.Paid}`, null); 
    expect(paidInvoice?.header.status).eq(InvoiceStatus.Paid);


    
  }).timeout(60000); 
});

