import { expect } from "chai";

import {
  SalaxySsoService,
} from "../_utils/SalaxySsoService";

// Configuration
import * as config from "../_configs/config.json";
import { PartnerCompanyAccountInfo } from "@salaxy/core";

// Create ajax
const ssoService = new SalaxySsoService(config);

describe("Federate", () => {
  it("should retrieve/create a company using a created access token", async () => {
     // Create assertion token for self
     const assertionToken = ssoService.getAssertionToken(config.salaxyAccountId);
     // Use assertion token to generate a new access token for self
     const tokenMessage = await ssoService.getToken(assertionToken);
     // Configure the API client with the token
     const ajax = ssoService.getAjax(tokenMessage.access_token);
     // Post data to API
     const company: PartnerCompanyAccountInfo = await ajax.postJSON("/v02/api/partner/company",
      {
         officialId: config.sampleBusinessId,
        //partnerAccountId:,
        //contactFirstName: "John",
        //contactLastName: "Snow",
        //email: "winter@salaxy.com",
        //telephone: "+358401234567",
        });
    expect(company || null).not.null;
  }).timeout(60000);
  it("should be able to create access token for the other company", async () => {
    // the next line basically does the same as the test above.
    const company = await ssoService.assureCompanyAccount({officialId: config.sampleBusinessId});

    // Check isAuthorized, if partnerAccountId is used
    const isAuthorized = company.isAuthorized;

    // Create assertion token for the company using Salaxy account id
    const assertionToken = ssoService.getAssertionToken(company.accountId);
    // Request a new token for the company using the assertion token
    const tokenMessage = await ssoService.getToken(assertionToken);
    // Return the received token
    const accessToken = tokenMessage.access_token;

    expect(accessToken || null).not.null;
  }).timeout(60000);
});

