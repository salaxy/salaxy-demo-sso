import { expect } from "chai";

import {
  SalaxySsoService,
} from "../_utils/SalaxySsoService";

// Configuration
import * as config from "../_configs/config.json";

// Create ajax
const ssoService = new SalaxySsoService(config);

describe("Salary slip", () => {
  
  it("should get pdf for salary", async () => {
    const ajax = await ssoService.assureCompanyAndGetAjax({officialId: config.sampleBusinessId});

    const filter = `otherPartyInfo/officialId eq '${config.sampleWorkerOfficialId}' and status eq 'paymentSucceeded'`;
    const list = await ajax.getJSON(`/v03-rc/api/calculations/all?$filter=${encodeURIComponent(filter)}`);
    
    expect(list.items.length).above(0);

    const base64 =  await ajax.getJSON(`/v02/api/reports/assure-pdf/salaryslip?ids=${list.items[0].id}`);

    console.log(base64);
    expect(base64 || null).not.null;
    
  }).timeout(60000); 
});

