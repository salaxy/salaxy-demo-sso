import { expect } from "chai";

import { ODataResult, WorkerListItem, PayrollDetails,} from "@salaxy/core";

import {
  SalaxySsoService,
} from "../_utils/SalaxySsoService";

// Configuration
import * as config from "../_configs/config.json";

const externalSalaryData = [
  {
    workerExternalId : "260688-9875", // can be officialId or system specific id
    rowType: "monthlySalary",
    count: 1,
    price: 3000
  },
  {
    workerExternalId : "260688-9875",
    rowType: "phoneBenefit",
    count: 1,
    price: 20
  },
  {
    workerExternalId : "300198-930E",
    rowType: "monthlySalary",
    count: 1,
    price: 4000
  },
  {
    workerExternalId : "300198-930E",
    rowType: "phoneBenefit",
    count: 1,
    price: 25
  }
]

// Create ajax

const ssoService = new SalaxySsoService(config);

describe("Payroll", () => {
  
  it("should create a new payroll from external salary data", async () => {
    const ajax = await ssoService.assureCompanyAndGetAjax({officialId: config.sampleBusinessId});

   // Get all workers with employment data
    const allEmployments: WorkerListItem[] = [];
    let workersUrl = "/v03-rc/api/accounts/workers";
    while(workersUrl) {
      const odataResult = (await ajax.getJSON(workersUrl)) as ODataResult<WorkerListItem>;
      allEmployments.push(...odataResult.items);
      workersUrl = odataResult.nextPageLink;
    }
    expect(allEmployments.length, "All employments").above(0);

    // Helper function for resolving employment
    const resolveEmploymentId = (id) => {
      if (!id) {
        return null;
      }
      return allEmployments.find( (x) =>  (x as any).externalId == id  || x.otherPartyInfo.officialId == id )?.id;
    }

    // Check that all employments exist 
    for(const externalRow of externalSalaryData) {
      expect(resolveEmploymentId(externalRow.workerExternalId) || null, "Employment for " + externalRow.workerExternalId ).not.null; 
    }

    // Create payroll
    let payroll: PayrollDetails = {
      input: {
         title: "Palkat",
         // salaryDate
      }
    }
    payroll = await ajax.postJSON("/v03-rc/api/payroll", payroll);
    expect(payroll?.id, "Payroll id").not.null;

    // Add employments
    const employmentIds = externalSalaryData
    // ids only
    .map(x => resolveEmploymentId(x.workerExternalId))
    // distinct values only
    .filter(
      (value, index, array) => array.indexOf(value) === index
     );
    payroll = await ajax.postJSON("/v03-rc/api/payroll/add-employment?ids=" + employmentIds.join(","), payroll);
    expect(payroll, "Payroll after employment addition").not.null;

    // Group external data by workerExternalId
    const externalDataGroups = externalSalaryData.reduce((previous, current) =>
    ((previous[current.workerExternalId] ??= []).push(current), previous), {});

    // Add rows to payroll calculations
    for(const externalDataGroupKey in externalDataGroups) {
      const employmentId = resolveEmploymentId(externalDataGroupKey);
      const calc = payroll.calcs.find( (x) => x.worker.employmentId == employmentId);
      expect(calc, "Calculation for " + externalDataGroupKey).not.null;

      // remove all default rows, this is optional if default rows are wanted
      calc.rows.splice(0, calc.rows.length);

      // add rows
      for(const externalRow of externalDataGroups[externalDataGroupKey]) {
        calc.rows.push( {
          // mapping
          rowType: externalRow.rowType,
          price: externalRow.price,
          count: externalRow.count,
        })
      }

      // Save calculation using calculation api (faster withour payroll refresh)
      await  ajax.postJSON("/v02/api/calculations", calc); 
      // Or Save calculation using payroll calculation api and refresh payroll
      // await  ajax.postJSON(`/v03-rc/api/payroll/${payroll.id}/calcs`, calc); 
    }

    // Refresh payroll
    payroll = await ajax.getJSON(`/v03-rc/api/payroll/${payroll.id}`);

    expect(payroll, "Refreshed payroll").not.null;

    // Direct to site
    const token = await ssoService.getToken(ssoService.getAssertionToken(payroll.owner));
    console.log("CTRL-click to open", `https://test-www.palkkaus.fi/company/#/payroll/details/${payroll.id}&access_token=${token.access_token}`);

  }).timeout(60000);
});

