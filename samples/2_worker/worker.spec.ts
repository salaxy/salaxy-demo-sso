import { expect } from "chai";

import { EmploymentRelationTaxcard, EmploymentRelationType, WorkerAccount, PensionCalculation} from "@salaxy/core";

import {
  SalaxySsoService,
} from "../_utils/SalaxySsoService";

// Configuration
import * as config from "../_configs/config.json";

// Create ajax
const ssoService = new SalaxySsoService(config);

describe("Worker", () => {
  it("should list all employments", async () => {
    const ajax = await ssoService.assureCompanyAndGetAjax({officialId: config.sampleBusinessId});
    // OData api
    const list = await ajax.getJSON("/v03-rc/api/accounts/workers")
    expect(list || null).not.null;
  }).timeout(60000);; 

  it("should create/update a worker", async () => {
    const ajax = await ssoService.assureCompanyAndGetAjax({officialId: config.sampleBusinessId});
    // Check employment existence
    const filter = `otherPartyInfo/officialId eq '${config.sampleWorkerOfficialId}'`;
    const list = await ajax.getJSON(`/v03-rc/api/accounts/workers?$filter=${encodeURIComponent(filter)}`);

    expect(list.items.length).below(2);
    const worker = list.items.length == 1 ? 
      (await ajax.getJSON(`/v02/api/accounts/workers/${list.items[0].otherId}`)) as WorkerAccount :
      {
        officialPersonId: config.sampleWorkerOfficialId,
        avatar: {
          firstName: "Sam",
          lastName: "Sample",
        },
        contact: {
          street: "Ansionkatu 1",
          city: "PALKKALA",
          countryCode: "fi",
          postalCode: "01337",
          email: "sam.sample@palkkaus.fi"
        },
        ibanNumber: "FI9517453000181869",
        employment: {
          type: EmploymentRelationType.Compensation,
          pensionCalculation: PensionCalculation.Compensation,
          taxcard: EmploymentRelationTaxcard.Auto,
          work: {
            occupationCode: "71150" // Kirvesmies
          },
        }
      } as WorkerAccount;

    const updatedWorker = await ajax.postJSON("/v02/api/accounts/workers", worker);

    expect(updatedWorker || null).not.null;
  }).timeout(60000);
});

