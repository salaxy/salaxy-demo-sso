These tests can be run using the test-script from command line.

For example, a single test:  ./samples/0_hello/hello.spec.ts

`npm run test ./samples/0_hello/hello.spec.ts`

All tests in a directory: ./samples/0_hello

`npm run test ./samples/0_hello/*.spec.ts`