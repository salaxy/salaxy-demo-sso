import { expect } from "chai";

import {
  AjaxNode,
} from "@salaxy/node";

// Configuration
import * as config from "../_configs/config.json";

// Create ajax
const ajax = new AjaxNode();

before( () => {
  // Configure
    ajax.serverAddress = config.apiServer;
  }
)

describe("Hello", () => {
  it("should return the passed message", async () => {
    const msg = "hello";
    const result = await ajax.getJSON("/test/hello?message=" + encodeURIComponent(msg));
    expect(result).eq(msg);
  }).timeout(60000);
});

